## Clone project
git clone https://gitlab.com/poopoopoo/reacttopro.git <br>
cd server_express <br>
npm install

## Run project
cd project <br>
cd server_express <br>
node server.js

## Structure project
node_modules => เก็บ library ต่าง <br>
public => เก็บไฟล์ index.html  และไฟล์ต่างๆ ที่ใช้ประกอบในการรันโปรเจคเพื่อแสดงผล <br>
src => โฟลเดอร์หลักที่เราจะใช้สร้างไฟล์ต่างๆ ที่เกี่ยวกับโปรเจคของเรา (source code) <br>
(src)pages => จะเก็บหน้าเพจที่แสดงผล <br>
(src)components => จะใช้เก็บ components ต่างๆ เพื่อประกอบกับเพจ <br>
.gitignore => ไฟล์นี้จะเป็นตัวบ่งบอกว่า เราไม่ต้องการไฟล์ หรือโฟลเดอร์ไหนเข้าสู่ระบบ git <br>
package.json => ไฟล์นี้จะรวม dependencies ต่างๆ ของ npm ที่เราต้องติดตั้งใช้งาน <br>
README.md => ไฟล์ที่อธิบายโปรเจค หรือคู่มือสั้น ๆ