const express = require('express')
const bodyParser = require('body-parser')
const app = express()

// get value form submit
app.use(bodyParser.urlencoded({ extended: true }))
// get value json
app.use(bodyParser.json({ type: '*/*' }))

// middleware handle route order
app.use('/order',(req, res, next) => {
    console.log('middle ware for order')
    next()
})

app.get('/', (req, res) => {
    res.send('Root Page')
})

app.get('/member', (req, res) => {
    res.send('Member Page')
})

app.get('/order', (req, res) => {
    res.send('Order Page')
})

app.get('/order/:orderId', (req, res) => {
    res.send('Order Page Id=' + req.params.orderId)
})

app.post('/order', (req, res) => {
    console.log(req.body)
    res.json({ status: 'ok' })
})

// run server express with port 8000
app.listen(8000, () => {
    console.log('ready on http://localhost:8000')
})