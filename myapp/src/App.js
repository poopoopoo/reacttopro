import React,{Component} from 'react'
import './App.css'
import Header from './Header'
import FbButton from './fbButton'

class App extends Component{

  state = { like: 0 }

  componentWillMount() {
    console.log('App: willMount')
  }

  componentDidMount() {
    console.log('App: didMount')
  }

  render(){
    console.log('App: render')
    return (
      <div>
        <Header title="I am Header" like={this.state.like} />
        <h1>Like: {this.state.like}</h1>
        <FbButton handleClick={this.onLike} caption="คลิกบวก Like" />
      </div>
    )
  }

  onLike = () => {
    // ถ้าต้องการเปลี่ยนค่าใน state ควรทำแบบนี้
    this.setState(prevState => {
      return {
        like: prevState.like + 1
      }
    })
  }

  onDislike = () => {
    // setState แบบเดิม จะใช้กรณีที่ไม่ต้องการเปลี่ยนแปลงค่า เช่น true, false 
    this.setState({ dislike: this.state.dislike + 1 })
  }

}

export default App;
