import React, {Component} from 'react'

class Header extends Component{

    // จะทำงานก่อนการ render ใช้เพื่อ config ค่าเริ่มต้นบางอย่าง
    // แต่ส่วนมากก็จะใช้ state เพราะก็เป็นการระบุค่าเริ่มต้นเหมือนกัน
    componentWillMount() {
        console.log('Header: willMount')
    }
    
    // จะทำงานเมื่อ render เสร็จแล้ว
    componentDidMount() {
        console.log('Header: didMount')
    }

    // จะทำงานก็ต่อเมื่อ component นี้ จะโดน re-render ใหม่ โดยสามารถบอกได้ว่าจะให้ re-render ใหม่หรือไม่ โดย return true or false เพื่อ performance
    // nextProps ใช้เปรียบเทียบค่า props ที่ส่งเข้ามา กับ props ปัจจุบันที่มีอยู่
    // nextState ใช้เปรียบเทียบค่า state ที่ส่งเข้ามา กับ state ปัจจุบันที่มีอยู่
    // ห้ามสั่ง setState เพราะจะทำให้เกิดการ infinity loop
    // shouldComponentUpdate(nextProps, nextState) {
    //     return false
    // }

    // จะทำงานก็ต่อเมื่อ component ได้รับ props เข้ามาใหม่ ก่อนที่ component นั้นจะทำการ re-render (ครั้งแรกที่ render เสร็จจะยังไม่ทำงาน)
    componentWillReceiveProps(nextProps) {
        console.log('nextProps', nextProps.like, 'props', this.props.like)
    }

    // componentWillUpdate(nextProps, nextState)
    // จะทำงานก็ต่อเมื่อ component ได้รับ props or state ใหม่มา ใช้เขียนเงื่อนไขเพื่อทำอะไรบางอย่างก่อน re-render 
    // ห้ามสั่ง setState เพราะจะทำให้เกิดการ infinity loop

    // componentDidUpdate(prevProps, prevState)
    // จะทำงานหลังจากเกิดการเปลี่ยนแปลง component แต่จะไม่ถูกเรียกในการ render ครั้งแรก
    // ใช้ตรวจสอบการเปลี่ยนแปลงหลังจาก render เสร็จแล้ว

    // componentWillUnmount()
    // จะทำงานก่อนที่ component นั้นจะโดน unmount และ destroy หรือ ลบ 
    // ส่วนใหญ่ใช้ reset ค่าต่างๆ แต่ห้าม setState 

    render(){
        console.log('Header: render')
        return(
            <div>
                <h1>{this.props.title} Like {this.props.like}</h1>
            </div>
        )
    }
}

export default Header